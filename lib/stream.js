var isArray = require('./util').isArray

module.exports = {
  /**
   * Streaming array helper
   *
   * @param {Stream} data (optional)
   */
  array: function streamArray (stream) {
    if (!stream) return function () {}
    var first = true

    return function _streamArray (data, end) {
      var json = JSON.stringify(data, true, 2)

      if (first) {
        stream.write('[\n')
        first = false
      }

      if (isArray(data)) {
        json = json.slice(1, -1)
      }

      if (end) {
        stream.end(json + ']')
      } else {
        stream.write(json + ',')
      }
    }
  },

  /**
   * Streaming object helper
   *
   * @param {Stream} data (optional)
   * @return {Function}
   */
  object: function streamObject (stream) {
    if (!stream) return function () {}

    return function _streamObject (data, end) {
      var json = JSON.stringify(data, true, 2)

      if (end) {
        stream.end(json)
      } else {
        stream.write(json)
      }
    }
  },

  waitCb: function streamCallback (stream, fn) {
    fn(function (err) {
      if (err) stream.emit('error', err)
    })
  }
}
